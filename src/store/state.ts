// state
import { prayersCalc, dayCalc } from 'prayer-timetable-lib'

// @ts-ignore
import timetable from '../config/timetable'
// @ts-ignore
import settings from '../config/settings'

const prayers = prayersCalc(
  timetable,
  settings,
  settings.jamaahShow,
  'Europe/Dublin'
)

settings.join === '0' && (settings.join = false)

const day = dayCalc(0, 0, {
  hijrioffset: settings.hijrioffset ? settings.hijrioffset : 0,
})

const state = {
  prayers,
  day,
  settings,
  update: {
    refreshed: 0,
    success: false,
    online: true,
    event: '',
  },
}

export default state
