// @ts-ignore
import axios from 'axios'
import localforage from 'localforage'

import { format } from 'date-fns'

import checkIfNeeded from './checkIfNeeded'
// @ts-ignore

// @ts-ignore
import { log, online, url } from '../../config/settings'

const API_URI = url.settings

/**
 * **********
 * ONLINE
 * **********
 */

const update = async (state: any, force: boolean) => {
  // console.log('run online')

  let { settings } = state
  const { update } = state

  const needed =
    checkIfNeeded(update.refreshed, update.success, settings.updateInterval) ||
    force

  if (needed) {
    try {
      const { data: newsettings } = await axios.get(API_URI, {
        method: 'GET',
        // mode: 'cors',
        // crossdomain: true,
        // withCredentials: true,
      })

      settings = { ...newsettings }
      update.refreshed = new Date()
      update.success = true

      await localforage.setItem('settings', settings)
      await localforage.setItem('refreshed', update.refreshed.valueOf())

      log && console.log('refreshed:', format(new Date(), 'HH:mm:ss'))
    } catch (error) {
      log && console.log('### SET ###', error)

      update.success = false
      log && console.log('error fetching update', error)
    }
  } else {
    // in case not needed
    update.success = true
    log && console.log('update not needed')
  }

  const payload = {
    settings,
    update,
  }

  return payload
}

/**
 * **********
 * LOCAL
 * **********
 */
const local = async (state: any) => {
  // console.log('run forage')

  let settings = { ...state.settings }
  const update = state.update

  if (online) {
    try {
      settings = await localforage.getItem('settings')
      update.refreshed = await localforage.getItem('refreshed')

      log && console.log('got storredsettings', await settings)
      log && console.log('got storredupdate', await update.refreshed)
    } catch (error) {
      /* eslint-disable prefer-destructuring */
      log && console.log('failed geting local storage', error)
    }
  } else {
    log && console.log('else')
  }
  /* eslint-disable babel/no-unused-expressions */
  // logging && console.log('got api settings', newsettings)

  const payload = {
    settings,
    update,
  }
  return payload
}

/**
 * **********
 * EXPORTS
 * **********
 */

export default update
export { update, local }
