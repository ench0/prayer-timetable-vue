import types from './types'
import { prayersCalc, dayCalc } from 'prayer-timetable-lib'
import checkOverlay from './reducers/checkOverlay'

// import { getLocal, getUpdate } from './reducers/getUpdate'
// @ts-ignore
import settings from '../config/settings'
// @ts-ignore
import timetable from '../config/timetable'
// @ts-ignore
import { log } from '../config/settings'

// Mutations
const mutations = {
  [types.TICK](state: any) {
    state.prayers = {
      ...prayersCalc(
        timetable,
        state.settings,
        state.settings.jamaahshow || true,
        'Europe/Dublin'
      ),
    }
    state.day = {
      ...dayCalc(0, 0, {
        hijrioffset: settings.hijrioffset ? settings.hijrioffset : 0,
      }),
    }
    state.update = {
      ...state.update,
      online: navigator.onLine,
      event: checkOverlay(state),
    }
  },
  [types.GET_LOCAL](state: any, payload: any) {
    log && console.log('run getLocal in mutations')
    // console.log('PAYLOAD', payload)

    payload.settings &&
      (payload.settings.join === '0' && (payload.settings.join = false))

    state.settings = {
      ...state.settings,
      ...payload.settings,
    }
    state.update = {
      ...state.update,
      ...payload.update,
    }
  },
  [types.GET_UPDATE](state: any, payload: any) {
    log && console.log('run getUpdate in mutations')
    // console.log('STATE', state)
    // console.log('PAYLOAD', payload)
    payload.settings.join === '0' && (payload.settings.join = false)

    state.settings = {
      ...state.settings,
      ...payload.settings,
    }
    state.update = {
      ...state.update,
      ...payload.update,
    }
  },
}

export default mutations
