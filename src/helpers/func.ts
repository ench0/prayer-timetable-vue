/*eslint-disable*/
import { toHijri } from 'hijri-converter' // toGregorian
import { format, addHours, getMinutes, getHours, getSeconds } from 'date-fns'

const wordCount = (str: string) => str.split(' ').filter(n => n !== '').length

const appendZero = (unit: number) => {
  if (unit < 10) {
    return `0${unit}`
  }
  return `${unit}`
}

// returns {hy, hm, hd}
const hijriConvert = (now: any) => {
  const { year, month, day } = {
    year: parseInt(format(now, 'yyyy'), 10),
    month: parseInt(format(now, 'M'), 10),
    day: parseInt(format(now, 'd'), 10),
  }
  // const result = toHijri(1987, 3, 1)

  const hijri = toHijri(year, month, day)
  // console.log(hijri.hm)

  return hijri
}

// returns '1 Muharram 1440'
const hijriDate = (now: any) => {
  const hijri = hijriConvert(now)

  let month

  switch (hijri.hm) {
    case 1:
      month = 'Muharram'
      break
    case 2:
      month = 'Safar'
      break
    case 3:
      month = 'Rabi’ al-awwal'
      break
    case 4:
      month = 'Rabi’ al-thani'
      break
    case 5:
      month = 'Jumada al-awwal'
      break
    case 6:
      month = 'Jumada al-thani'
      break
    case 7:
      month = 'Rajab'
      break
    case 8:
      month = 'Sha’ban'
      break
    case 9:
      month = 'Ramadan'
      break
    case 10:
      month = 'Shawwal'
      break
    case 11:
      month = 'Dhu al-Qi’dah'
      break
    case 12:
      month = 'Dhu al-Hijjah'
      break
    default:
      month = ''
  }

  const result = `${hijri.hd} ${month} ${hijri.hy}`
  // console.log(hijri.hm)

  return result
}

const convertToDuration = (secondsAmount: number) => {
  const normalizeTime = (time: string): string =>
    time.length === 1 ? `0${time}` : time

  const SECONDS_TO_MILLISECONDS_COEFF = 1000
  const MINUTES_IN_HOUR = 60

  const milliseconds = secondsAmount * SECONDS_TO_MILLISECONDS_COEFF

  const date = new Date(milliseconds)
  const timezoneDiff = date.getTimezoneOffset() / MINUTES_IN_HOUR
  const dateWithoutTimezoneDiff = addHours(date, timezoneDiff)

  const hours = normalizeTime(String(getHours(dateWithoutTimezoneDiff)))
  const minutes = normalizeTime(String(getMinutes(dateWithoutTimezoneDiff)))
  const seconds = normalizeTime(String(getSeconds(dateWithoutTimezoneDiff)))

  const hoursOutput = hours !== '00' ? `${hours}:` : ''

  return `${hoursOutput}${minutes}:${seconds}`
}

export default { wordCount }
export { appendZero, wordCount, hijriConvert, hijriDate, convertToDuration }
